package com.example.tiku6_1.net;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;


import org.json.JSONException;
import org.json.JSONObject;

;

/**
 * Created by Administrator on 2019/9/15 0015.
 */

public class VolleyTo extends Thread  {
    private String Url="http://"+AppClient.getIP()+":8080/traffic/";
    private JSONObject jsonObject=new JSONObject();
    private int Time;
    private boolean isLoop;
    private Dialog mDialog;
    private VolleyLo volleyLo;
    private Handler handler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            if (mDialog!=null&&mDialog.isShowing()){
                mDialog.dismiss();
            }
            return false;
        }
    });

    public VolleyTo setUrl(String url) {
        Url += url;
        return this;
    }

    public VolleyTo setJsonObject(String k, Object v) {
        try {
            jsonObject.put(k,v);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

    public VolleyTo setTime(int time) {
        Time = time;
        return this;
    }

    public VolleyTo setLoop(boolean loop) {
        isLoop = loop;
        return this;
    }

    public VolleyTo setmDialog(Context context) {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        View view= LayoutInflater.from(context).inflate(R.layout.ts_dialog,null,false);
        builder.setView(view);
        mDialog=builder.show();
        return this;
    }

    public VolleyTo setVolleyLo(VolleyLo volleyLo) {
        this.volleyLo = volleyLo;
        return this;
    }

    @Override
    public void run() {
        super.run();
        do {
            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    volleyLo.onResponse(jsonObject);
                    handler.sendEmptyMessageDelayed(0x001,1000);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    volleyLo.onErrorResponse(volleyError);
                    handler.sendEmptyMessageDelayed(0x001,1000);
                }
            });

            AppClient.setRequestQueue(jsonObjectRequest);
            try {
                Thread.sleep(Time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while (isLoop);
    }
}
