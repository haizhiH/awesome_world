package com.example.tiku6_1.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class GJCX_Fragment1 extends Fragment {
    private BarChart barChart;
    private BarDataSet dataSet1, dataSet2;
    private List<BarEntry> barEntries1, barEntries2;
    private BarData data;
    private List<Integer> colors1, colors2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gjcx_fragment1, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        barChart = getView().findViewById(R.id.barChart1);
        barEntries1 = new ArrayList<>();
        barEntries2 = new ArrayList<>();
        colors1 = new ArrayList<>();
        colors2 = new ArrayList<>();
        setVolley();
    }

    private void setVolley() {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_bus_stop_distance")
                .setJsonObject("UserName", "user1")
                .setLoop(true)
                .setTime(3000)
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        if (jsonObject.has("RESULT")) {
                            try {
                                colors1.clear();
                                colors2.clear();
                                barEntries1.clear();
                                barEntries2.clear();
                                if (jsonObject.getString("RESULT").equals("S")) {
                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("中医院站"));
                                    JSONArray jsonArray1 = new JSONArray(jsonObject.getString("联想大厦站"));
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(1);
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    JSONObject jsonObject4 = jsonArray1.getJSONObject(1);
                                    int first = jsonObject1.getInt("distance");
                                    int second = jsonObject2.getInt("distance");
                                    int third = jsonObject3.getInt("distance");
                                    int forth = jsonObject4.getInt("distance");
                                    if (first > second) {
                                        colors1.add(Color.YELLOW);
                                        colors2.add(Color.GREEN);
                                    } else {
                                        colors1.add(Color.GREEN);
                                        colors2.add(Color.YELLOW);
                                    }
                                    if (third > forth) {
                                        colors1.add(Color.YELLOW);
                                        colors2.add(Color.GREEN);
                                    } else {
                                        colors1.add(Color.GREEN);
                                        colors2.add(Color.YELLOW);
                                    }
                                    barEntries1.add(new BarEntry(0, first));
                                    barEntries1.add(new BarEntry(1, third));
                                    barEntries2.add(new BarEntry(0, second));
                                    barEntries2.add(new BarEntry(1, forth));
                                    setDarData();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }).start();
    }

    private void setDarData() {
        dataSet1 = new BarDataSet(barEntries1, "");
        dataSet1.setColors(colors1);
        dataSet2 = new BarDataSet(barEntries2, "");
        dataSet2.setColors(colors2);
        data = new BarData(dataSet1, dataSet2);
        float groupSpace = 1f; //柱状图组之间的间距
        float barSpace = 0f;
        data.setBarWidth(0.1f);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setTextColor(Color.WHITE);
        setY();
        data.groupBars(-0.7f, groupSpace, barSpace);
        barChart.setData(data);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setTouchEnabled(false);
        barChart.invalidate();

    }

    private void setY() {
        YAxis yAxisR = barChart.getAxisRight();
        yAxisR.setEnabled(false);
        YAxis yAxisL = barChart.getAxisLeft();
        yAxisL.setDrawAxisLine(true);
        yAxisL.setTextColor(Color.WHITE);
    }
}
