package com.example.tiku6_1.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.GJCXViewPager;
import com.example.tiku6_1.bean.SSHJ;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;
import com.example.tiku6_1.util.SimpData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Create by 张瀛煜 on 2019/10/15
 */
public class GJCX_Fragment2 extends Fragment {
    private ViewPager viewPager;
    private LinearLayout layout;
    private List<SSHJ> sshjs;
    private AppClient appClient;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gjcx_fragmnet2, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        setViewPager();
        setShow();
    }

    private List<Fragment> fragments;

    private void setViewPager() {
        fragments = new ArrayList<>();
        fragments.add(new CO_Fragment());
        fragments.add(new WD_Fragment());
        fragments.add(new SD_Fragment());

        viewPager.setAdapter(new GJCXViewPager(getFragmentManager(), fragments));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                for (int j = 0 ; j<layout.getChildCount();j++){
                    ImageView imageView = (ImageView) layout.getChildAt(j);
                    if (j==i){
                        imageView.setImageResource(R.drawable.page_indicator_focused);
                    }else {
                        imageView.setImageResource(R.drawable.page_indicator_unfocused);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setShow() {
        layout.removeAllViews();
        for (int i = 0; i < fragments.size(); i++) {
            ImageView imageView = new ImageView(getContext());
            if (i == 0) {
                imageView.setImageResource(R.drawable.page_indicator_focused);
            } else {
                imageView.setImageResource(R.drawable.page_indicator_unfocused);
            }
            imageView.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
            layout.addView(imageView);
        }
    }

    private void initView() {
        viewPager = getView().findViewById(R.id.viewPager);
        layout = getView().findViewById(R.id.layout);
        appClient = (AppClient) getActivity().getApplication();
        sshjs = appClient.getSshjs();
    }

}
