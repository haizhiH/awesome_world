package com.example.tiku6_1.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.WdlkAdapter;
import com.example.tiku6_1.bean.Wdlk;
import com.example.tiku6_1.dialog.HldDialog;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

@SuppressLint("ValidFragment")
public class WdlkFragment extends Fragment {

    private ListView mListView;
    private List<Wdlk> mList=new ArrayList<>();
    private WdlkAdapter mAdapter;
    private Context context;
    private FragmentManager fragmentManager;
    private Dialog dialog;
    private AppClient mApp;
    private Handler handler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            if (message.what==0x001){
                mAdapter.notifyDataSetChanged();
            }else {
                if (dialog!=null&&dialog.isShowing()){
                    dialog.dismiss();
                }
            }
            return false;
        }
    });

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wdlk_fragment,container,false);
    }

    public WdlkFragment(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView=getView().findViewById(R.id.wdlk_list);
        mApp= (AppClient) getActivity().getApplication();
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        View view= LayoutInflater.from(context).inflate(R.layout.ts_dialog,null,false);
        builder.setView(view);
        dialog=builder.show();
        initData();
    }

    private void initData(){
        if (mApp.getUserName().equals("user1")){
            mList.add(new Wdlk("路口1",23,3,20,20,23,"绿灯","红灯",true,true,true));
            mList.add(new Wdlk("路口2",27,5,22,27,22,"红灯","绿灯",true,true,true));
            mList.add(new Wdlk("路口3",33,4,29,4,3,"黄灯","绿灯",true,true,true));
            mList.add(new Wdlk("路口4",26,7,19,6,6,"红灯","黄灯",true,true,true));
            mList.add(new Wdlk("路口5",17,3,14,17,14,"红灯","绿灯",true,true,true));
        }else {
            mList.add(new Wdlk("路口1",23,3,20,20,23,"绿灯","红灯",true,true,false));
            mList.add(new Wdlk("路口2",27,5,22,27,22,"红灯","绿灯",true,true,false));
            mList.add(new Wdlk("路口3",33,4,29,4,3,"黄灯","绿灯",true,true,false));
            mList.add(new Wdlk("路口4",26,7,19,6,6,"红灯","黄灯",true,true,false));
            mList.add(new Wdlk("路口5",17,3,14,17,14,"红灯","绿灯",true,true,false));
        }
        mAdapter=new WdlkAdapter(context,R.layout.wdlk_item,mList);
        mListView.setAdapter(mAdapter);
        mAdapter.setData(new WdlkAdapter.SetData() {
            @Override
            public void setdata(final int index, int hao) {
                if (hao==2){
                    Wdlk wdlk=mList.get(index);
                    wdlk.setHvalue(30);
                    wdlk.setHzt("绿灯");
                    wdlk.setHe(false);
                    wdlk.setSvalue(30);
                    wdlk.setSzt("红灯");
                    wdlk.setSh(false);
                    mList.set(index,wdlk);
                }else if (hao==3){
                    Wdlk wdlk=mList.get(index);
                    wdlk.setHvalue(30);
                    wdlk.setHzt("红灯");
                    wdlk.setHe(false);
                    wdlk.setSvalue(30);
                    wdlk.setSzt("绿灯");
                    wdlk.setSh(false);
                    mList.set(index,wdlk);
                }else if (hao==1){
                    HldDialog dialog=new HldDialog(context,(index+1)+"");
                    dialog.setData(new HldDialog.SetData() {
                        @Override
                        public void setdata(int Hod, int Hud, int Lvd) {
                            Wdlk wdlk=mList.get(index);
                            wdlk.setHod(Hod);
                            wdlk.setHud(Hud);
                            wdlk.setLvd(Lvd);
                            mList.set(index,wdlk);
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                    dialog.show(fragmentManager,"dialog");
                }
            }
        });
        new Thread(){
            @Override
            public void run() {
                super.run();
                do {
                    for (int i=0;i<mList.size();i++){
                        Wdlk wdlk= mList.get(i);
                        String hed=wdlk.getHzt();
                        if (hed.equals("红灯")){
                            int value=wdlk.getHvalue();
                            if (value>0){
                                value--;
                                wdlk.setHvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setHzt("黄灯");
                                wdlk.setHvalue(wdlk.getHud());
                                wdlk.setHe(true);
                                mList.set(i,wdlk);
                            }
                        }else if (hed.equals("黄灯")){
                            int value=wdlk.getHvalue();
                            if (value>0){
                                value--;
                                wdlk.setHvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setHe(true);
                                wdlk.setHzt("绿灯");
                                wdlk.setHvalue(wdlk.getLvd());
                                mList.set(i,wdlk);
                            }
                        }else if (hed.equals("绿灯")){
                            int value=wdlk.getHvalue();
                            if (value>0){
                                value--;
                                wdlk.setHvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setHe(true);
                                wdlk.setHzt("红灯");
                                wdlk.setHvalue(wdlk.getHod());
                                mList.set(i,wdlk);
                            }
                        }
                        String shd=wdlk.getSzt();
                        if (shd.equals("红灯")){
                            int value=wdlk.getSvalue();
                            if (value>0){
                                value--;
                                wdlk.setSvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setSh(true);
                                wdlk.setSzt("黄灯");
                                wdlk.setSvalue(wdlk.getHud());
                                mList.set(i,wdlk);
                            }
                        }else if (shd.equals("黄灯")){
                            int value=wdlk.getSvalue();
                            if (value>0){
                                value--;
                                wdlk.setSvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setSh(true);
                                wdlk.setSzt("绿灯");
                                wdlk.setSvalue(wdlk.getLvd());
                                mList.set(i,wdlk);
                            }
                        }else if (shd.equals("绿灯")){
                            int value=wdlk.getSvalue();
                            if (value>0){
                                value--;
                                wdlk.setSvalue(value);
                                mList.set(i,wdlk);
                            }else {
                                wdlk.setSh(true);
                                wdlk.setSzt("红灯");
                                wdlk.setSvalue(wdlk.getHod());
                                mList.set(i,wdlk);
                            }
                        }
                    }
                    handler.sendEmptyMessage(0x001);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }while (true);
            }
        }.start();
        handler.sendEmptyMessageDelayed(0x002,2000);
    }
}
