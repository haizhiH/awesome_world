package com.example.tiku6_1.fragment;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Random;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

@SuppressLint("ValidFragment")
public class DlhjFragment extends Fragment {

    private TextView mPm;
    private TextView mTs;
    private VideoView videoView;
    private TextView mGz;
    private TextView mTs2;
    private TextView mValue;
    private TextView mValue2;
    private VolleyTo volleyTo;
    private Context context;


    public DlhjFragment(Context context) {
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dlhj_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        setVolley();
    }

    private void setVolley() {
        volleyTo=new VolleyTo();
        volleyTo.setUrl("get_all_sense").setJsonObject("UserName","user1").setmDialog(context).setTime(3000).setLoop(true).setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("RESULT")){
                    try {
                        String result=jsonObject.getString("RESULT");
                        if (result.equals("S")){
                            int pm=jsonObject.getInt("pm25");
                            mPm.setText("PM2.5当前值："+pm);
                            if (pm>80){
                                mTs.setText("友情提示：PM2.5大于80，不适合出行。");
                                Date date=new Date(System.currentTimeMillis());
                                showNotificationView("PM2.5大于80，不适合出行。",1);
                                videoView.setVisibility(View.VISIBLE);
                                videoView.start();
                            }else {
                                mTs.setText("友情提示：适合出行");
                                videoView.setVisibility(View.GONE);
                            }
                            int gz=jsonObject.getInt("illumination");
                            mGz.setText("光照强度当前值："+gz);
                            if (gz>3900){
                                mTs2.setText("友情提示：光照较强，出行请戴墨镜");
                                Date date=new Date(System.currentTimeMillis());
                                showNotificationView("光照较强，出行请戴墨镜。",2);
                            }else if (gz<1200){
                                mTs2.setText("友情提示：光照较弱，出行请开灯");
                                Date date=new Date(System.currentTimeMillis());
                                showNotificationView("光照较弱，出行请开灯。",3);
                            }else {
                                mTs2.setText("友情提示：适合出行");
                            }
                            mValue.setPadding((gz/10),0,0,0);
                            mValue2.setPadding((gz/10)-50,0,0,0);
                            mValue2.setText(gz+"");
                            Random random=new Random();
                            int dl=random.nextInt(5);
                            int lh=random.nextInt(6)+1;
                            if (dl>3){
                                Date date=new Date(System.currentTimeMillis());
                                showNotificationView(lh+"号路口处于拥挤堵塞状态，请选择合适的路线",4);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void showNotificationView(String message,int id){
        NotificationCompat.Builder builder=new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("智能交通管理系统")
                .setContentText(message)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis());
        NotificationManager notificationManager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id,builder.build());
    }

    private void initData() {
        videoView.setVideoURI(Uri.parse("android.resource://"+getActivity().getPackageName()+"/" + R.raw.xiaoshipin));
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });
    }

    private void initView() {
        mPm=getView().findViewById(R.id.dl_pm);
        mTs=getView().findViewById(R.id.dl_ts);
        videoView=getView().findViewById(R.id.dl_video);
        mGz=getView().findViewById(R.id.dl_gzdq);
        mTs2=getView().findViewById(R.id.dl_ts2);
        mValue=getView().findViewById(R.id.dl_value);
        mValue2=getView().findViewById(R.id.dl_value2);
    }

    @Override
    public void onStop() {
        super.onStop();
        volleyTo.setLoop(false);
        volleyTo=null;
    }
}
