package com.example.tiku6_1.bean;

public class zi {
    private String yuanyin,didian,shijain,yuan, yuanyin1,didian1,shijain1,yuan1;

    public zi(String yuanyin, String didian, String shijain, String yuan, String yuanyin1, String didian1, String shijain1, String yuan1) {
        this.yuanyin = yuanyin;
        this.didian = didian;
        this.shijain = shijain;
        this.yuan = yuan;
        this.yuanyin1 = yuanyin1;
        this.didian1 = didian1;
        this.shijain1 = shijain1;
        this.yuan1 = yuan1;
    }

    @Override
    public String toString() {
        return "zi{" +
                "yuanyin='" + yuanyin + '\'' +
                ", didian='" + didian + '\'' +
                ", shijain='" + shijain + '\'' +
                ", yuan='" + yuan + '\'' +
                ", yuanyin1='" + yuanyin1 + '\'' +
                ", didian1='" + didian1 + '\'' +
                ", shijain1='" + shijain1 + '\'' +
                ", yuan1='" + yuan1 + '\'' +
                '}';
    }

    public String getYuanyin() {
        return yuanyin;
    }

    public void setYuanyin(String yuanyin) {
        this.yuanyin = yuanyin;
    }

    public String getDidian() {
        return didian;
    }

    public void setDidian(String didian) {
        this.didian = didian;
    }

    public String getShijain() {
        return shijain;
    }

    public void setShijain(String shijain) {
        this.shijain = shijain;
    }

    public String getYuan() {
        return yuan;
    }

    public void setYuan(String yuan) {
        this.yuan = yuan;
    }

    public String getYuanyin1() {
        return yuanyin1;
    }

    public void setYuanyin1(String yuanyin1) {
        this.yuanyin1 = yuanyin1;
    }

    public String getDidian1() {
        return didian1;
    }

    public void setDidian1(String didian1) {
        this.didian1 = didian1;
    }

    public String getShijain1() {
        return shijain1;
    }

    public void setShijain1(String shijain1) {
        this.shijain1 = shijain1;
    }

    public String getYuan1() {
        return yuan1;
    }

    public void setYuan1(String yuan1) {
        this.yuan1 = yuan1;
    }
}
