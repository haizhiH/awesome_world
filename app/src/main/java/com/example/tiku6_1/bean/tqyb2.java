package com.example.tiku6_1.bean;

public class tqyb2 {
    private String name,zhi,zhunagtai,yanse;

    public tqyb2(String name, String zhi, String zhunagtai, String yanse) {
        this.name = name;
        this.zhi = zhi;
        this.zhunagtai = zhunagtai;
        this.yanse = yanse;
    }

    @Override
    public String toString() {
        return "tqyb2{" +
                "name='" + name + '\'' +
                ", zhi='" + zhi + '\'' +
                ", zhunagtai='" + zhunagtai + '\'' +
                ", yanse='" + yanse + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZhi() {
        return zhi;
    }

    public void setZhi(String zhi) {
        this.zhi = zhi;
    }

    public String getZhunagtai() {
        return zhunagtai;
    }

    public void setZhunagtai(String zhunagtai) {
        this.zhunagtai = zhunagtai;
    }

    public String getYanse() {
        return yanse;
    }

    public void setYanse(String yanse) {
        this.yanse = yanse;
    }
}
