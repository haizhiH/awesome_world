package com.example.tiku6_1.bean;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class Tcc {
    String Bh;
    String Name;
    String Kw;
    String Fl;
    String Jl;
    String Dd;
    boolean Ty;

    public Tcc() {
    }

    public Tcc(String bh, String name, String kw, String fl, String jl, String dd, boolean ty) {
        Bh = bh;
        Name = name;
        Kw = kw;
        Fl = fl;
        Jl = jl;
        Dd = dd;
        Ty = ty;
    }

    public String getBh() {
        return Bh;
    }

    public void setBh(String bh) {
        Bh = bh;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getKw() {
        return Kw;
    }

    public void setKw(String kw) {
        Kw = kw;
    }

    public String getFl() {
        return Fl;
    }

    public void setFl(String fl) {
        Fl = fl;
    }

    public String getJl() {
        return Jl;
    }

    public void setJl(String jl) {
        Jl = jl;
    }

    public String getDd() {
        return Dd;
    }

    public void setDd(String dd) {
        Dd = dd;
    }

    public boolean isTy() {
        return Ty;
    }

    public void setTy(boolean ty) {
        Ty = ty;
    }
}
