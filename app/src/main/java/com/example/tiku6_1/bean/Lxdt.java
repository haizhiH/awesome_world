package com.example.tiku6_1.bean;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class Lxdt {
    String Name;
    double Jd;
    double Wd;

    public Lxdt() {
    }

    public Lxdt(String name, double jd, double wd) {
        Name = name;
        Jd = jd;
        Wd = wd;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getJd() {
        return Jd;
    }

    public void setJd(double jd) {
        Jd = jd;
    }

    public double getWd() {
        return Wd;
    }

    public void setWd(double wd) {
        Wd = wd;
    }
}
