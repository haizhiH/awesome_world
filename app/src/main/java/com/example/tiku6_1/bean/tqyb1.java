package com.example.tiku6_1.bean;

public class tqyb1 {
    private String zt;

    public tqyb1(String zt) {
        this.zt = zt;
    }

    @Override
    public String toString() {
        return "tqyb1{" +
                "zt='" + zt + '\'' +
                '}';
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }
}
