package com.example.tiku6_1.bean;

/**
 *  "id": 1,
 *             "number": 1,
 *             "owner": "张三",
 *             "balance": 1295,
 *             "plate": "鲁A10001",
 *             "brand": "奔驰",
 *             "user": "user1"
 * Create by 张瀛煜 on 2019/10/19
 */
public class GRZX {
    private String brand,plate,balance;

    public GRZX(String brand, String plate, String balance) {
        this.brand = brand;
        this.plate = plate;
        this.balance = balance;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
