package com.example.tiku6_1.bean;

public class fu {
    private String fu;

    public fu(String fu) {
        this.fu = fu;
    }

    @Override
    public String toString() {
        return "fu{" +
                "fu='" + fu + '\'' +
                '}';
    }

    public String getFu() {
        return fu;
    }

    public void setFu(String fu) {
        this.fu = fu;
    }
}
