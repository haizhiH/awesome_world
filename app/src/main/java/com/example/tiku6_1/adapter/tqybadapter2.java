package com.example.tiku6_1.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.tqyb2;

import java.util.List;

public class tqybadapter2 extends BaseAdapter {
    private List<tqyb2> mtqyb;
    private Context context;
    public tqybadapter2(Context context,List<tqyb2> mtqyb)
    {
        this.context=context;
        this.mtqyb=mtqyb;
    }
    @Override
    public int getCount() {
        return mtqyb.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        tqyb2 t = mtqyb.get(i);
        View view1  =View.inflate(context, R.layout.tqyb_item2,null);
        TextView tv1 = view1.findViewById(R.id.name);
        TextView tv2  =view1.findViewById(R.id.shuju);
        TextView tv3 = view1.findViewById(R.id.zhuangtai1);
        ImageView im = view1.findViewById(R.id.tubiao1);
        tv1.setText(t.getName());
        tv2.setText(t.getZhi());
        tv3.setText(t.getZhunagtai());
        tv3.setTextColor(Color.parseColor("#"+t.getYanse()));
        if (i==0)
        {
            im.setImageResource(R.drawable.light_index);
        }
        if (i==1)
        {
            im.setImageResource(R.drawable.air_index);
        }
        if (i==2)
        {
            im.setImageResource(R.drawable.sport_index);
        }
        if (i==3)
        {
            im.setImageResource(R.drawable.dress_index);
        }
        if (i==4)
        {
            im.setImageResource(R.drawable.cold_index);
        }
        if (i==5)
        {
            im.setImageResource(R.drawable.clear_car_index);
        }
        return view1;
    }
}
