package com.example.tiku6_1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.Lxzn;

import java.util.List;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class LxznAdapter extends ArrayAdapter<Lxzn> {

    private int mLayout;

    public LxznAdapter(@NonNull Context context, int resource, @NonNull List<Lxzn> objects) {
        super(context, resource, objects);
        mLayout=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Lxzn lxzn=getItem(position);
        View view= LayoutInflater.from(getContext()).inflate(mLayout,parent,false);
        TextView dz=view.findViewById(R.id.zn_dz);
        TextView dl=view.findViewById(R.id.zn_dl);
        TextView je=view.findViewById(R.id.zn_js);
        dz.setText(lxzn.getDz());
        dl.setText(lxzn.getDl());
        je.setText(lxzn.getXc());
        return view;
    }
}
