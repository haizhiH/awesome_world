package com.example.tiku6_1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.Wdlk;

import java.util.List;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class WdlkAdapter extends ArrayAdapter<Wdlk> {

    private int mLayout;

    public WdlkAdapter(@NonNull Context context, int resource, @NonNull List<Wdlk> objects) {
        super(context, resource, objects);
        mLayout=resource;
    }

    public interface SetData{
        void setdata(int index, int lx);
    }

    private SetData data;

    public void setData(SetData data) {
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Wdlk wdlk=getItem(position);
        View view= LayoutInflater.from(getContext()).inflate(mLayout,parent,false);
        TextView Lk=view.findViewById(R.id.lk_name);
        TextView Lvd=view.findViewById(R.id.lk_lvd);
        TextView Hud=view.findViewById(R.id.lk_hud);
        TextView Hod=view.findViewById(R.id.lk_hod);
        Button Pz=view.findViewById(R.id.lk_pz);
        TextView Hzt=view.findViewById(R.id.lk_he_zt);
        TextView HValue=view.findViewById(R.id.lk_he_value);
        TextView HValue2=view.findViewById(R.id.lk_he_value2);
        TextView Szt=view.findViewById(R.id.lk_sh_zt);
        TextView SValue=view.findViewById(R.id.lk_sh_value);
        TextView SValue2=view.findViewById(R.id.lk_sh_value2);
        Button Hbt=view.findViewById(R.id.lk_he_bt);
        Button Sbt=view.findViewById(R.id.lk_sh_bt);
        Lk.setText(wdlk.getLk());
        Lvd.setText(wdlk.getLvd()+"");
        Hud.setText(wdlk.getHud()+"");
        Hod.setText(wdlk.getHod()+"");
        Pz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.setdata(position,1);
            }
        });
        Hbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.setdata(position,2);
            }
        });
        Sbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.setdata(position,3);
            }
        });
        HValue.setText(wdlk.getHvalue()+"");
        HValue2.setText(wdlk.getHvalue()+"");
        Hzt.setText(wdlk.getHzt());
        if (wdlk.getHzt().equals("绿灯")){
            HValue.setBackgroundResource(R.drawable.lvdeng);
        }else if (wdlk.getHzt().equals("黄灯")){
            HValue.setBackgroundResource(R.drawable.huangdeng);
        }else if (wdlk.getHzt().equals("红灯")){
            HValue.setBackgroundResource(R.drawable.hongdeng);
        }
        SValue.setText(wdlk.getSvalue()+"");
        SValue2.setText(wdlk.getSvalue()+"");
        Szt.setText(wdlk.getSzt());
        if (wdlk.getSzt().equals("绿灯")){
            SValue.setBackgroundResource(R.drawable.lvdeng);
        }else if (wdlk.getSzt().equals("黄灯")){
            SValue.setBackgroundResource(R.drawable.huangdeng);
        }else if (wdlk.getSzt().equals("红灯")){
            SValue.setBackgroundResource(R.drawable.hongdeng);
        }
        if (wdlk.isQx()){
            Pz.setVisibility(View.VISIBLE);
        }else {
            Pz.setVisibility(View.GONE);
        }
        Hbt.setEnabled(wdlk.isHe());
        Sbt.setEnabled(wdlk.isSh());
        return view;
    }
}
