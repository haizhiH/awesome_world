package com.example.tiku6_1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.GRZX;

import java.util.List;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class GRAX_ADapter extends ArrayAdapter<GRZX> {
    private int layout;

    public GRAX_ADapter(Context context, int resource, List<GRZX> objects) {
        super(context, resource, objects);
        layout = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GRZX grzx = getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(layout, parent, false);
        ImageView my_iamge = view.findViewById(R.id.my_iamge);
        TextView my_info = view.findViewById(R.id.my_info);
        switch (grzx.getBrand()){
            case "奔驰":
                my_iamge.setImageResource(R.drawable.benchi);
                break;
            case "宝马":
                my_iamge.setImageResource(R.drawable.zhonghua);
                break;
            case "奥迪":
                my_iamge.setImageResource(R.drawable.audi);
                break;
            case "中华":
                my_iamge.setImageResource(R.drawable.benchi);
                break;
        }
        my_info.setText(grzx.getBrand()+"       余额:"+grzx.getBalance());
        return view;
    }
}
