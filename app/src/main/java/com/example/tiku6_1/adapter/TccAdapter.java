package com.example.tiku6_1.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.Tcc;

import java.util.List;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class TccAdapter extends ArrayAdapter<Tcc> {

    private int mLayout;

    public TccAdapter(@NonNull Context context, int resource, @NonNull List<Tcc> objects) {
        super(context, resource, objects);
        mLayout=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Tcc tcc=getItem(position);
        View view= LayoutInflater.from(getContext()).inflate(mLayout,parent,false);
        TextView Bh=view.findViewById(R.id.tcc_id);
        TextView Name=view.findViewById(R.id.tcc_name);
        TextView Kw=view.findViewById(R.id.tcc_kw);
        TextView Jl=view.findViewById(R.id.tcc_jl);
        ImageView Jt=view.findViewById(R.id.tcc_jt);
        TextView Tv=view.findViewById(R.id.tcc_tv);
        LinearLayout Layout=view.findViewById(R.id.tcc_layout);
        Bh.setText(tcc.getBh());
        Name.setText(tcc.getName());
        Kw.setText("空车位"+tcc.getKw()+"个，停车费"+tcc.getFl());
        Jl.setText(tcc.getDd()+","+tcc.getJl()+"米");
        if (tcc.isTy()){
            Tv.setText("关闭");
            Layout.setBackgroundColor(Color.parseColor("#cccccc"));
            Jt.setVisibility(View.GONE);
            Tv.setVisibility(View.VISIBLE);
        }else {
            Jt.setVisibility(View.VISIBLE);
            Tv.setVisibility(View.GONE);
        }
        return view;
    }
}
