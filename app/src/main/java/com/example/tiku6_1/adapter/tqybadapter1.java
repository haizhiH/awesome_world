package com.example.tiku6_1.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.tqyb1;

import java.util.List;

public class tqybadapter1 extends BaseAdapter {
    private List<tqyb1> mtqyb;
    private Context context;
    private String[] tq={"10月19日","10月20日","10月21日","10月22日","10月23日","10月24日"};
    private String[] xq={"(今日) 周六","周日","周一","周二","周三","周四"};
    public tqybadapter1(Context context,List<tqyb1> mtqyb)
    {
        this.context=context;
        this.mtqyb=mtqyb;
    }
    @Override
    public int getCount() {
        return mtqyb.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        tqyb1 t = mtqyb.get(i);
        View view1  =View.inflate(context, R.layout.tqyb_item1,null);
        TextView tv1 = view1.findViewById(R.id.riiq);
        TextView tv2  =view1.findViewById(R.id.xingqi);
        TextView tv3 = view1.findViewById(R.id.zhuangtai);
        ImageView im = view1.findViewById(R.id.tubiao);
        tv1.setText(tq[i]);
        tv2.setText(xq[i]);
        tv3.setText(t.getZt());
        if (t.getZt().equals("晴"))
        {
            im.setImageResource(R.drawable.sun);
        }
        if (t.getZt().equals("小雨"))
        {
            im.setImageResource(R.drawable.rain);
        }
        if (t.getZt().equals("阴"))
        {
            im.setImageResource(R.drawable.cloudy);
        }

        return view1;
    }
}
