package com.example.tiku6_1.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.fu;
import com.example.tiku6_1.bean.zi;

import java.util.List;

public class cyadapter extends BaseExpandableListAdapter {
    private List<fu> mfu;
    private List<zi> mzi;
    public cyadapter(List<fu> mfu,List<zi> mzi)
    {
        this.mfu=mfu;
        this.mzi=mzi;
    }
    @Override
    public int getGroupCount() {
        return mfu.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        fu f = mfu.get(i);
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fu_item,null,false);
        TextView tv = view.findViewById(R.id.fu);
        tv.setText(f.getFu());
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        zi z = mzi.get(i);
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.zi_item,null,false);
        TextView tv = view.findViewById(R.id.zi1);
        TextView tv1 = view.findViewById(R.id.zi2);
        TextView tv2 = view.findViewById(R.id.zi3);
        TextView tv3 = view.findViewById(R.id.zi4);

        TextView tv4 = view.findViewById(R.id.zi11);
        TextView tv5 = view.findViewById(R.id.zi21);
        TextView tv6 = view.findViewById(R.id.zi31);
        TextView tv7 = view.findViewById(R.id.zi41);
        tv.setText(z.getYuanyin());
        tv1.setText(z.getDidian());
        tv2.setText(z.getShijain());
        tv3.setText(z.getYuan());

        tv4.setText(z.getYuanyin1());
        tv5.setText(z.getDidian1());
        tv6.setText(z.getShijain1());
        tv7.setText(z.getYuan1());
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
