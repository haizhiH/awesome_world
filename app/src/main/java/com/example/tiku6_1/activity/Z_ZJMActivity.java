package com.example.tiku6_1.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class Z_ZJMActivity extends BaseActivity {
    private TextView bus_2, bus_1, dlhj_1, dlhj_2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zjm_layouy);
        initView();
        setVolley();
    }
    private void setVolley() {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_all_sense")
                .setJsonObject("UserName", "user1")
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                                if (jsonObject.optString("RESULT").equals("S")) {
                                    dlhj_1.setText("PM2.5:" + jsonObject.optString("pm25") + "ug/m³,温度:" + jsonObject.optString("temperature") + "℃");
                                    dlhj_2.setText("湿度:" + jsonObject.optString("humidity") + "℃,CO2:" + jsonObject.optString("co2") + "ug/m³");
                                    setVolley_2();
                                }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                })
                .start();
    }

    private void setVolley_2() {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_bus_stop_distance")
                .setJsonObject("UserName", "user1")
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        if (jsonObject.has("RESULT")) {
                                if (jsonObject.optString("RESULT").equals("S")) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("中医院站");
                                    bus_1.setText("1号公交" + jsonArray.optJSONObject(0).optString("distance") + "m,2号公交" + jsonArray.optJSONObject(1).optString("distance") + "m");
                                    JSONArray jsonArray1 = jsonObject.optJSONArray("联想大厦站");
                                    bus_2.setText("1号公交" + jsonArray1.optJSONObject(0).optString("distance") + "m,2号公交" + jsonArray1.optJSONObject(1).optString("distance") + "m");
                                }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }).start();
    }

    private void initView() {
        left();
        setTitle("主界面");
        addActivity(this);
        bus_2 = findViewById(R.id.bus_2);
        bus_1 = findViewById(R.id.bus_1);
        dlhj_1 = findViewById(R.id.dlhj_1);
        dlhj_2 = findViewById(R.id.dlhj_2);
    }
}
