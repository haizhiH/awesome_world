package com.example.tiku6_1.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.amap.api.maps.model.RouteOverlay;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.DriveStep;
import com.amap.api.services.route.RideRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.WalkRouteResult;
import com.android.volley.VolleyError;
import com.autonavi.ae.gmap.gloverlay.BaseRouteOverlay;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.Lxdt;
import com.example.tiku6_1.bean.Lxzn;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class LxdtActivity extends AppCompatActivity implements RouteSearch.OnRouteSearchListener {

    private Spinner mQd;
    private Spinner mZd;
    private TextView mSs;
    private AMap aMap;
    private MapView mapView;
    private Double Jd;
    private Double Wd;
    private String Name;
    private AppClient mApp;
    private List<Lxdt> mList;
    private List<Lxzn> LxList;
    private TextView mXq;
    private VolleyTo volleyTo;
    private RouteSearch routeSearch;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lxdt_layout);
        Jd = getIntent().getDoubleExtra("jd", 0);
        Wd = getIntent().getDoubleExtra("wd", 0);
        Name = getIntent().getStringExtra("name");
        mApp = (AppClient) getApplication();
        LxList = mApp.getLxList();
        initView();
        initializeWithState(savedInstanceState);
        initData();
        setVolley();
        setValue();
        initLister();
    }

    private void initLister() {
        mSs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qd = mQd.getSelectedItem().toString().trim();
                String zd = mZd.getSelectedItem().toString().trim();
                LatLonPoint start = null;
                LatLonPoint end = null;
                for (int i = 0; i < mList.size(); i++) {
                    Lxdt lxdt = mList.get(i);
                    if (lxdt.getName().equals(qd)) {
                        start = new LatLonPoint(lxdt.getJd(), lxdt.getWd());
                    }
                    if (lxdt.getName().equals(zd)) {
                        end = new LatLonPoint(lxdt.getJd(), lxdt.getWd());
                    }
                }
                RouteSearch.FromAndTo fromAndTo = new RouteSearch.FromAndTo(start, end);
                RouteSearch.DriveRouteQuery query = new RouteSearch.DriveRouteQuery(fromAndTo, 0, null, null, "");
                routeSearch.calculateDriveRouteAsyn(query);
            }
        });
        mXq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LxdtActivity.this, LxznActivity.class);
                String qd = mQd.getSelectedItem().toString().trim();
                String zd = mZd.getSelectedItem().toString().trim();
                intent.putExtra("qd", qd);
                intent.putExtra("zd", zd);
                startActivity(intent);
            }
        });
    }

    private void setValue() {
        final String[] array1 = {"鼓楼大街", "北京成林医院", "景山公园", "月坛公园", "四平园小区", "中国科学院大学"};
        mQd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String zd1 = array1[i];
                String zd2 = mZd.getSelectedItem().toString().trim();
                if (zd1.equals(zd2)) {
                    showAlertDialog("起点终点不可选择相同站点");
                    mQd.setSelection(i > 0 ? i - 1 : i + 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mZd.setSelection(1);
        mZd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String zd1 = array1[i];
                String zd2 = mQd.getSelectedItem().toString().trim();
                if (zd1.equals(zd2)) {
                    showAlertDialog("起点终点不可选择相同站点");
                    mZd.setSelection(i > 0 ? i - 1 : i + 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage(message);
        builder.setPositiveButton("确定", null);
        builder.show();
    }

    private void setVolley() {
        volleyTo = new VolleyTo();
        mList = new ArrayList<>();
        volleyTo.setUrl("get_site_data").setJsonObject("UserName", "user1").setmDialog(this).setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("RESULT")) {
                    try {
                        String result = jsonObject.getString("RESULT");
                        if (result.equals("S")) {
                            String body = jsonObject.getString("ROWS_DETAIL");
                            JSONArray jsonArray = new JSONArray(body);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                mList.add(new Lxdt(jsonObject1.getString("site"), jsonObject1.getDouble("longitude"), jsonObject1.getDouble("latitude")));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void initData() {
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(Jd, Wd)));
        aMap.moveCamera(CameraUpdateFactory.zoomTo(11));
        LatLng latLng = new LatLng(Jd, Wd);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(Name).snippet("");
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.marker_self)));
        markerOptions.setFlat(true);
        aMap.addMarker(markerOptions);
        Toast.makeText(this, "已为您定位到" + Name + "停车场", Toast.LENGTH_LONG).show();
    }

    private void initializeWithState(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        if (aMap == null) aMap = mapView.getMap();
    }

    private void initView() {
        mQd = findViewById(R.id.lx_qd);
        mZd = findViewById(R.id.lx_zd);
        mSs = findViewById(R.id.lx_ss);
        mapView = findViewById(R.id.map_view);
        mXq = findViewById(R.id.lx_xq);
        routeSearch = new RouteSearch(this);
        routeSearch.setRouteSearchListener(this);
        TextView tv = findViewById(R.id.title);
        ImageView change = findViewById(R.id.change);
        tv.setText("离线地图");
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {

    }

    @Override
    public void onDriveRouteSearched(DriveRouteResult driveRouteResult, int i) {
        LxList.clear();
        aMap.clear();
        List<DrivePath> drivePaths = driveRouteResult.getPaths();
        List<LatLng> latLngs = new ArrayList<>();
        for (DrivePath drivePath : drivePaths) {
            List<DriveStep> driveSteps = drivePath.getSteps();
            for (DriveStep step : driveSteps) {
                LxList.add(new Lxzn(step.getRoad(), step.getInstruction(), step.getAction()));
                List<LatLonPoint> latLonPoints = step.getPolyline();
                for (LatLonPoint latLonPoint : latLonPoints) {
                    LatLng latLng = new LatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude());
                    latLngs.add(latLng);
                }
            }
        }
        aMap.addPolyline(new PolylineOptions().addAll(latLngs).width(10).color(Color.BLUE));
    }

    @Override
    public void onWalkRouteSearched(WalkRouteResult walkRouteResult, int i) {

    }

    @Override
    public void onRideRouteSearched(RideRouteResult rideRouteResult, int i) {

    }
}
