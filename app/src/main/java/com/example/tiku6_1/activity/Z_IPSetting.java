package com.example.tiku6_1.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.util.Show_Dialog;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class Z_IPSetting extends AppCompatActivity {
    private Button save;
    private EditText ip_2, ip_1, ip_3, ip_4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ip_layout);

        initView();
        Liseter1(ip_1, 1);
        Liseter1(ip_2, 0);
        Liseter1(ip_3, 0);
        Liseter1(ip_4, 0);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ip_1.getText().length() == 0) {
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                if (ip_2.getText().length() == 0) {
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                if (ip_3.getText().length() == 0) {
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                if (ip_4.getText().length() == 0) {
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                /*int ip1 = Integer.parseInt(ip_1.getText().toString().trim());
                if (!(ip1 >= 0 && ip1 < 256)) {
                    ip_1.setText("");
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                int ip2 = Integer.parseInt(ip_1.getText().toString().trim());
                if (!(ip2 >= 1 && ip2 < 256)) {
                    ip_2.setText("");
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }
                int ip3 = Integer.parseInt(ip_1.getText().toString().trim());
                if (!(ip3 >= 1 && ip3 < 256)) {
                    ip_3.setText("");
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;

                }
                int ip4 = Integer.parseInt(ip_1.getText().toString().trim());
                if (!(ip4 >= 1 && ip4 < 256)) {
                    ip_4.setText("");
                    Show_Dialog.Show(Z_IPSetting.this, "请输入正确的IP地址");
                    return;
                }*/
                AppClient.setIP(ip_1.getText().toString() + "." + ip_2.getText().toString() + "." + ip_3.getText().toString() + "." + ip_4.getText().toString());
                Show_Dialog.Show(Z_IPSetting.this, "保存成功");
            }
        });

    }

    private void Liseter1(final EditText editText, final int min) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    if (Integer.parseInt(s.toString()) >= min && Integer.parseInt(s.toString()) <= 255) {

                    } else {
                        Toast.makeText(Z_IPSetting.this, "您输入的IP有误", Toast.LENGTH_SHORT).show();
                        editText.setText("");
                    }
                }
            }
        });
    }

    private void initView() {
        TextView title = findViewById(R.id.title);
        title.setText("网络设置");
        ImageView change = findViewById(R.id.change);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        save = findViewById(R.id.save);
        ip_2 = findViewById(R.id.ip_2);
        ip_1 = findViewById(R.id.ip_1);
        ip_3 = findViewById(R.id.ip_3);
        ip_4 = findViewById(R.id.ip_4);
    }
}
