package com.example.tiku6_1.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.dialog.WLTS_Dialog;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class BaseActivity extends AppCompatActivity {
    private SlidingMenu menu;
    private boolean is = true;
    private static boolean isSuccess = true;
    private static List<Activity> list = new ArrayList<>();
    private WLTS_Dialog dialog;
    private AppClient appClient;

    public static void setIsSuccess(boolean isSuccess) {
        BaseActivity.isSuccess = isSuccess;
    }

    public void left() {
        menu = new SlidingMenu(this);
        menu.setBehindWidth(300);
        menu.setFadeDegree(0.35f);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        menu.setMode(SlidingMenu.LEFT);
        menu.setMenu(R.layout.menu);
        ListView listView = menu.findViewById(R.id.menu_listView);
        final List<String> list = new ArrayList<>();
        list.add("我的交通");
        list.add("公交查询");
        list.add("我的座驾");
        list.add("路况分析");
        list.add("用户停车");
        list.add("天气预报");
        list.add("创意题");
        list.add("退出登录");
        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Class clazz = null;
                switch (position) {
                    case 1:
                        clazz = Z_GJCXActivity.class;
                        break;
                    case 5:
                        clazz = S_TQYBActivity.class;
                        break;
                    case 3:
                        clazz = S_LKFXActivity.class;
                        break;
                    case 4:
                        clazz = AYhtcActivity.class;
                        break;
                    case 2:
                        clazz = Z_GRAXActivity.class;
                        break;
                    case 0:
                        clazz = AWdjtActivity.class;
                        break;
                    case 6:
                        clazz = S_CYActivity.class;
                        break;
                    case 7:
                        clazz = S_DLActivity.class;
                        appClient = (AppClient) getApplication();
                        appClient.setUserName("");
                        appClient.setPassWord("");
                        break;
                }

                startActivity(new Intent(BaseActivity.this, clazz));
                finish();
            }
        });
        MyThread();
        LinearLayout setting_ip = menu.findViewById(R.id.setting_ip);
        setting_ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, Z_IPSetting.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        isSuccess = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isSuccess = false;
    }

    private void MyThread() {
        dialog = new WLTS_Dialog();
        new Thread() {
            @Override
            public void run() {
                super.run();
                do {
                    if (!isNetwork(BaseActivity.this)) {
                        if (isSuccess) {
                            dialog.show(getSupportFragmentManager(), "aaa");
                            isSuccess = false;
                            Log.i("aaa", "ddddd");
                        }
                    }
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } while (is);
            }
        }.start();
    }

    public boolean isNetwork(Context context) {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        if (mNetworkInfo != null) {
            return mNetworkInfo.isAvailable();
        }
        return false;
    }

    public void setTitle(String a) {
        TextView textView = findViewById(R.id.title);
        textView.setText(a);
        ImageView change = findViewById(R.id.change);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.showMenu();
            }
        });
        appClient = (AppClient) getApplication();
        TextView ttitle2 = findViewById(R.id.title1);
        ttitle2.setText("当前用户" + appClient.getUserName());
    }

    public static void addActivity(Activity activity) {
        list.add(activity);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        for (int i = 0; i < list.size(); i++) {
            Activity activity = list.get(i);
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
