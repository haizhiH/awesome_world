package com.example.tiku6_1.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.LxznAdapter;
import com.example.tiku6_1.bean.Lxzn;

import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 2019/10/19 0019.
 */
public class LxznActivity extends AppCompatActivity {

    private ListView mListView;
    private TextView mQd;
    private TextView mZd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lxzn_layout);
        initView();
        initData();
    }

    private void initData() {
        AppClient mApp = (AppClient) getApplication();
        List<Lxzn> mList = mApp.getLxList();
        mQd.setText(getIntent().getStringExtra("qd"));
        mZd.setText(getIntent().getStringExtra("zd"));
        mListView.setAdapter(new LxznAdapter(this, R.layout.lxzn_item, mList));
    }

    private void initView() {
        mListView = findViewById(R.id.lxzn_list);
        mQd = findViewById(R.id.zn_qd);
        mZd = findViewById(R.id.zn_zd);
        TextView Fz = findViewById(R.id.zn_fz);
        TextView Lc = findViewById(R.id.zn_lc);
        Random random = new Random();
        int a = random.nextInt(16);
        Fz.setText(a + "分钟");
        Lc.setText((a + 2) + "公里");
        TextView tv = findViewById(R.id.title);
        tv.setText("路线指南");
        findViewById(R.id.change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
