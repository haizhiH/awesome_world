package com.example.tiku6_1.activity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.animation.AlphaAnimation;
import com.amap.api.maps.model.animation.Animation;
import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.TccAdapter;
import com.example.tiku6_1.bean.Tcc;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class AYhtcActivity extends BaseActivity implements AMap.OnMarkerClickListener {

    private AMap aMap;
    private MapView mapView;
    private ImageButton locationBtn;
    private ImageButton markerBtn;
    private ImageButton layerBtn;
    private ImageButton lineBtn;
    private ListView mListView;
    private List<Tcc> mList;
    private List<LatLng> latLngs = new ArrayList<>();
    private MarkerOptions firstCarOption, secondCarOption, thirdCarOption;
    private TccAdapter mAdapter;
    private List<Marker> markers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d_yhtc_layout);
        initView();
        initializeWithState(savedInstanceState);
        setVolley();
        initData();
        initLister();
        left();
        addActivity(this);
        setTitle("用户停车");
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_5, R.anim.anim_6);
    }

    private void initLister() {
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeMarkers();
                aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(39.942209, 116.384467)));
                aMap.moveCamera(CameraUpdateFactory.zoomTo(14));
                aMap.addMarker(new MarkerOptions()
                        .position(new LatLng(39.942209, 116.384467))
                        .title("什刹海")
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.marker_self)))
                        .setFlat(true));
                mListView.setVisibility(View.GONE);
            }
        });
        markerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aMap.setMyLocationEnabled(false);
                removeMarkers();
                aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(39.940294, 116.377787)));
                aMap.moveCamera(CameraUpdateFactory.zoomTo(14));
                addMarker();
                mListView.setVisibility(View.VISIBLE);
            }
        });
        layerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListView.setVisibility(View.GONE);
            }
        });
        lineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListView.setVisibility(View.GONE);
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tcc tcc = mList.get(i);
                if (tcc.isTy()) {
                    return;
                }

                Intent intent = new Intent(AYhtcActivity.this, TccxqActivity.class);
                intent.putExtra("name", tcc.getName());
                startActivity(intent);
            }
        });
    }


    private void addMarker() {
        Marker marker = aMap.addMarker(firstCarOption);
        Marker marker2 = aMap.addMarker(secondCarOption);
        Marker marker3 = aMap.addMarker(thirdCarOption);
        if (markers == null) markers = new ArrayList<>();
        markers.clear();
        markers.add(marker);
        markers.add(marker2);
        markers.add(marker3);
    }

    private void removeMarkers() {
        if (markers != null) {
            for (Marker marker : markers) {
                marker.remove();
            }
        }
    }

    private void initData() {
        aMap.setLoadOfflineData(true);
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(39.942209, 116.384467)));
        aMap.moveCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void setVolley() {
        VolleyTo volleyTo = new VolleyTo();
        mList = new ArrayList<>();
        volleyTo.setUrl("get_park_data").setJsonObject("UserName", "user1").setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("RESULT")) {
                    try {
                        String result = jsonObject.getString("RESULT");
                        if (result.equals("S")) {
                            mList.clear();
                            latLngs.clear();
                            String boyd = jsonObject.getString("ROWS_DETAIL");
                            Random random = new Random();
                            JSONArray jsonArray = new JSONArray(boyd);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                boolean root = !jsonObject1.getString("root").equals("对外开放");
                                mList.add(new Tcc(jsonObject1.getString("id"), jsonObject1.getString("parking_name"), random.nextInt(100) + "", jsonObject1.getString("price") + "元/" + jsonObject1.getString("rate_type"), random.nextInt(1000) + "", jsonObject1.getString("location"), root));
                                latLngs.add(new LatLng(jsonObject1.getDouble("longitude"), jsonObject1.getDouble("latitude")));
                            }
                            initMarkerData();
                            Collections.sort(mList, new Jlsx());
                            mAdapter = new TccAdapter(AYhtcActivity.this, R.layout.tcc_item, mList);
                            mListView.setAdapter(mAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void initMarkerData() {
        firstCarOption = new MarkerOptions()
                .position(latLngs.get(0))
                .title("南京学院停车场").snippet("栖霞区洋山北路1号")
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.marker_one)))
                .setFlat(true);

        secondCarOption = new MarkerOptions()
                .position(latLngs.get(1))
                .title("德州百货停车场").snippet("德城区三八东路7号")
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.marker_second)))
                .setFlat(true);

        thirdCarOption = new MarkerOptions()
                .position(new LatLng(39.947705, 116.39476))
                .title("德州职业停车场").snippet("德城区大学东路15号")
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.marker_thrid)))
                .setFlat(true);

        Animation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        long duration = 800L;
        alphaAnimation.setDuration(duration);
        alphaAnimation.setInterpolator(new BounceInterpolator());
        aMap.setOnMarkerClickListener(this);
    }

    private void initializeWithState(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        if (aMap == null) {
            aMap = mapView.getMap();
        }
    }

    private void initView() {
        mapView = findViewById(R.id.map_view);
        locationBtn = findViewById(R.id.location);
        layerBtn = findViewById(R.id.change_layer);
        markerBtn = findViewById(R.id.marker);
        lineBtn = findViewById(R.id.line_distance);
        mListView = findViewById(R.id.yhtc_list);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    private class Jlsx implements Comparator<Tcc> {

        @Override
        public int compare(Tcc tcc, Tcc t1) {
            return Integer.parseInt(tcc.getJl()) - Integer.parseInt(t1.getJl());
        }
    }
}
