package com.example.tiku6_1.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.tqybadapter1;
import com.example.tiku6_1.adapter.tqybadapter2;
import com.example.tiku6_1.bean.tqyb1;
import com.example.tiku6_1.bean.tqyb2;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class S_TQYBActivity extends BaseActivity {

    private TextView wd ,zt;
    private ImageView tb;
    private List<tqyb1> mtqyb;
    private String[] s,s1,s2;
    private tqybadapter1  mtqybadapter1;
    private GridView gridView;
    private LineChart lineChart;
    private LineDataSet dataSet,dataSet1;
    private LineData data;
    private List<Entry> mY,mY1;
    private List<ILineDataSet> iLineDataSets;
    private GridView gridView1;
    private List<tqyb2> mtqyb1;
    private tqybadapter2  mtqybadapter2;
    private String yi,er,san;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_tqybactivity);
        inview();
        huoqu();
        huoqu1();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_8, R.anim.anim_1);
    }
    private void setadapter1() {
        mtqybadapter2 = new tqybadapter2(this,mtqyb1);
        gridView1.setAdapter(mtqybadapter2);
    }

    private void huoqu1() {
        VolleyTo volleyTo= new VolleyTo();
        volleyTo.setUrl("get_all_sense").setJsonObject("UserName","user1").setTime(3000).setLoop(true).setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    mtqyb1.clear();
                    int zwx= jsonObject.getInt("illumination");
                    int gm= jsonObject.getInt("humidity");
                    int cy= jsonObject.getInt("temperature");
                    int yd= jsonObject.getInt("co2");
                    int kq= jsonObject.getInt("pm25");
                    if (zwx>0&&zwx<1000)
                    {
                        tqyb2 t1 = new tqyb2("紫外线指数",zwx+"","弱","4472c4");
                        mtqyb1.add(t1);
                    }else  if (zwx>=1000&&zwx<=3000)
                    {
                        tqyb2 t1 = new tqyb2("紫外线指数",zwx+"","中等","00b050");
                        mtqyb1.add(t1);
                    }else  if (zwx>3000)
                    {
                        tqyb2 t1 = new tqyb2("紫外线指数",zwx+"","强","ff0000");
                        mtqyb1.add(t1);
                    }

                    //********************************


                    if (kq>0&&kq<35)
                    {
                        tqyb2 t2 = new tqyb2("空气污染指数",kq+"","优","44dc68");
                        mtqyb1.add(t2);
                    }else  if (kq>=35&&kq<=75)
                    {
                        tqyb2 t2 = new tqyb2("空气污染指数",kq+"","良","92d050");
                        mtqyb1.add(t2);
                    }else  if (kq>75&&kq<115)
                    {
                        tqyb2 t2 = new tqyb2("空气污染指数",kq+"","轻度污染","ffff40");
                        mtqyb1.add(t2);
                    }
                    else  if (kq>=115&&kq<=150)
                    {
                        tqyb2 t2 = new tqyb2("空气污染指数",kq+"","中度污染","bf9000");
                        mtqyb1.add(t2);
                    }
                    else  if (kq>150)
                    {
                        tqyb2 t2 = new tqyb2("空气污染指数",kq+"","重度污染","993300");
                        mtqyb1.add(t2);
                    }
                    //***********

                    if (yd>0&&yd<3000)
                    {
                        tqyb2 t3 = new tqyb2("运动指数",yd+"","适宜","44dc68");
                        mtqyb1.add(t3);
                    }else  if (yd>=3000&&yd<6000)
                    {
                        tqyb2 t3 = new tqyb2("运动指数",yd+"","中","ffc000");
                        mtqyb1.add(t3);
                    }else   if (yd>6000)
                    {
                        tqyb2 t3 = new tqyb2("运动指数",yd+"","较不易","8149ac");
                        mtqyb1.add(t3);
                    }

                    //********************************


                    if (cy<12)
                    {
                        tqyb2 t4 = new tqyb2("穿衣指数",cy+"","冷","3462f4");
                        mtqyb1.add(t4);
                    }else  if (cy <= 21)
                    {
                        tqyb2 t4 = new tqyb2("穿衣指数",cy+"","舒适","92d050");
                        mtqyb1.add(t4);
                    }else  if (cy < 35)
                    {
                        tqyb2 t4 = new tqyb2("穿衣指数",cy+"","温暖","44dc68");
                        mtqyb1.add(t4);
                    }
                    else {
                        tqyb2 t4 = new tqyb2("穿衣指数",cy+"","热","ff0000");
                        mtqyb1.add(t4);
                    }

                    //********************************

                    if (gm<50)
                    {
                        tqyb2 t5 = new tqyb2("感冒指数",gm+"","较易发","ff0000");
                        mtqyb1.add(t5);
                    }else {
                        tqyb2 t5 = new tqyb2("感冒指数",gm+"","少发","ffff40");
                        mtqyb1.add(t5);
                    }

                    //********************************





                    //********************************
                    VolleyTo volleyTo  =new VolleyTo();
                    volleyTo.setUrl("get_weather_info").setJsonObject("UserName","user1").setVolleyLo(new VolleyLo()
                    {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
                                String arr = jsonObject.getString("ROWS_DETAIL");
                                JSONArray jsonArray  =new JSONArray(arr);
                                 yi = jsonArray.getJSONObject(0).getString("weather");
                                er = jsonArray.getJSONObject(1).getString("weather");
                                san = jsonArray.getJSONObject(2).getString("weather");

                                if (yi.equals("小雨")||er.equals("小雨"))
                                {
                                    tqyb2 t6 = new tqyb2("洗车指数 ","","不适宜 ","000000");
                                    mtqyb1.add(t6);
                                }else   if (san.equals("小雨"))
                                {
                                    tqyb2 t6 = new tqyb2("洗车指数 ","","不太适宜 ","000000");
                                    mtqyb1.add(t6);
                                }else
                                {
                                    tqyb2 t6 = new tqyb2("洗车指数 ","","适宜 ","000000");
                                    mtqyb1.add(t6);
                                }
                                setadapter1();
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    }).start();

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void addata() {
        mY.add(new Entry(0,Integer.parseInt(s1[0])));
        mY1.add(new Entry(0,Integer.parseInt(s2[0])));

        mY.add(new Entry(1,Integer.parseInt(s1[1])));
        mY1.add(new Entry(1,Integer.parseInt(s2[1])));

        mY.add(new Entry(2,Integer.parseInt(s1[2])));
        mY1.add(new Entry(2,Integer.parseInt(s2[2])));

        mY.add(new Entry(3,Integer.parseInt(s1[3])));
        mY1.add(new Entry(3,Integer.parseInt(s2[3])));

        mY.add(new Entry(4,Integer.parseInt(s1[4])));
        mY1.add(new Entry(4,Integer.parseInt(s2[4])));

        mY.add(new Entry(5,Integer.parseInt(s1[5])));
        mY1.add(new Entry(5,Integer.parseInt(s2[5])));
    }

    private void settu() {
        lineChart = findViewById(R.id.liechart);
        mtqyb = new ArrayList<>();
        mY = new ArrayList<>();
        mY1 = new ArrayList<>();

        addata();
        dataSet  =new LineDataSet(mY,"");
        dataSet1 = new LineDataSet(mY1,"");
        dataSet.setColor(Color.BLUE);
        dataSet1.setColor(Color.YELLOW);
        dataSet.setCircleColors(Color.BLUE);
        dataSet1.setCircleColors(Color.YELLOW);
        iLineDataSets = new ArrayList<>();
        iLineDataSets.add(dataSet);
        iLineDataSets.add(dataSet1);
        data =new LineData(iLineDataSets);
        lineChart.setData(data);
        dataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                DecimalFormat format  = new DecimalFormat("0");
                return format.format(value)+"°";
            }
        });
        dataSet1.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                DecimalFormat format  = new DecimalFormat("0");
                return format.format(value)+"°";
            }
        });
        dataSet.setDrawCircleHole(false);
        dataSet1.setDrawCircleHole(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.setDescription(null);
        lineChart.setTouchEnabled(false);
        lineChart.getAxisLeft().setEnabled(false);
        XAxis xAxis  =lineChart.getXAxis();
        xAxis.setEnabled(false);

    }

    private void setadapter() {
        mtqybadapter1 = new tqybadapter1(this,mtqyb);
        gridView.setAdapter(mtqybadapter1);
    }

    private void huoqu() {
        VolleyTo volleyTo  =new VolleyTo();
        volleyTo.setUrl("get_weather_info").setJsonObject("UserName","user1").setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    wd.setText(jsonObject.getString("temperature")+"度");
                    zt.setText(jsonObject.getString("weather"));
                    if (jsonObject.getString("weather").equals("阴"))
                    {
                        tb.setImageResource(R.drawable.cloudy);
                    }
                    if (jsonObject.getString("weather").equals("小雨"))
                    {
                        tb.setImageResource(R.drawable.rain);
                    }
                    if (jsonObject.getString("weather").equals("晴"))
                    {
                        tb.setImageResource(R.drawable.sun);
                    }
                    String arr=  jsonObject.getString("ROWS_DETAIL");
                    JSONArray jsonArray  =new JSONArray(arr);
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        mtqyb.add(new tqyb1(jsonObject1.getString("weather")));
                        s=jsonObject1.getString("interval").split("~");
                        s1[i]=s[0];
                        s2[i]=s[1];
                    }
                    setadapter();
                    settu();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void inview() {
        mtqyb1  =new ArrayList<>();
        gridView1  =findViewById(R.id.gridview1);
        s=new String[2];
        s1 = new String[6];
        s2 = new String[6];
        mtqyb = new ArrayList<>();
        wd = findViewById(R.id.wd);
        zt = findViewById(R.id.zt);
        gridView = findViewById(R.id.gridview);
        tb = findViewById(R.id.tb);
        left();
        addActivity(this);
        setTitle("天气预报");
    }
}
