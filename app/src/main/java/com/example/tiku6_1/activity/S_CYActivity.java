package com.example.tiku6_1.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.cyadapter;
import com.example.tiku6_1.bean.fu;
import com.example.tiku6_1.bean.zi;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

public class S_CYActivity extends BaseActivity {

    private ExpandableListView expandableListView;
    private TextView bt;
    private BarChart barChart;
    private BarDataSet dataSet;
    private BarData data;
    private List<BarEntry> mY;
    private List<String> mX;
    private cyadapter mcyadapter;
    private List<Integer> color;
    private List<fu> mfu;
    private List<zi> mzi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_cyactivity);
        inview();
        settu();
        setdianji();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_1, R.anim.anim_2);
    }

    private void setzhi(int i) {
        mfu.clear();
        mfu.add(new fu("车辆违章"));
        mfu.add(new fu("ETC消费"));
        mfu.add(new fu("停车场消费"));
        if (i == 0) {
            bt.setText("七月");
            mzi.clear();
            mzi.add(new zi("原因：超速行驶", "地点：北环路", "时间2019-7-1 8：20：02", "消费：100"
                    , "原因：酒驾", "地点：南环路", "时间2019-7-2 10：00：22", "消费：100"));

            mzi.add(new zi("原因：缴费", "地点：幸福路", "时间2019-7-3 11：20:2", "消费：50"
                    , "原因：缴费", "地点：建设路", "时间2019-7-4 21:20:2", "消费：100"));

            mzi.add(new zi("原因：停车", "地点：东环路", "时间2019-7-5 20：55：12", "消费：50"
                    , "原因：停车", "地点：大学路", "时间2019--6 10：02：02", "消费：60"));
            mcyadapter.notifyDataSetChanged();

        } else if (i == 1) {
            bt.setText("八月");
            mzi.clear();
            mzi.add(new zi("原因：逆向行驶", "地点：联想路", "时间2019-8-7 10：50：02", "消费：50"
                    , "原因：超速行驶", "地点：建设路", "时间2019-8-8 15：00：02", "消费：150"));

            mzi.add(new zi("原因：缴费", "地点：医院路", "时间2019-8-9 10：40：22", "消费：50"
                    , "原因：缴费", "地点：幸福路", "时间2019-8-14 10:00:22", "消费：50"));

            mzi.add(new zi("原因：停车", "地点：建设路", "时间2019-8-15 22:50:12", "消费：30"
                    , "原因：停车", "地点：大学路", "时间2019-8-16 10:02:12", "消费：20"));
            mcyadapter.notifyDataSetChanged();

        } else if (i == 2) {
            bt.setText("九月");
            mzi.clear();
            mzi.add(new zi("原因：违章停车", "地点：开发路", "时间2019-9-11 19：50：02", "消费：150"
                    , "原因：超速行驶", "地点：南环路", "时间2019-9-22 18：20：02", "消费：50"));

            mzi.add(new zi("原因：缴费", "地点：东环路", "时间2019-9-23 12：45：22", "消费：70"
                    , "原因：缴费", "地点：开发路", "时间2019-9-24 10：22：22", "消费：30"));

            mzi.add(new zi("原因：停车", "地点：幸福路", "时间2019-9-25 12：50：12", "消费：100"
                    , "原因：停车", "地点：大学路", "时间2019-9-16 15：02：02", "消费：20"));
            mcyadapter.notifyDataSetChanged();

        } else if (i == 3) {
            bt.setText("十月");
            mzi.clear();
            mzi.add(new zi("原因：逆向行驶", "地点：北环路", "时间2019-10-11 10：50：02", "消费：100"
                    , "原因：酒驾", "地点：幸福路", "时间2019-10-21 15：10：12", "消费：100"));

            mzi.add(new zi("原因：缴费", "地点：南湖路", "时间2019-10-13 12：40：20", "消费：20"
                    , "原因：缴费", "地点：联想路", "时间2019-10-24 10：10：22", "消费：30"));

            mzi.add(new zi("原因：停车", "地点：南环路", "时间2019-10-15 11：50：12", "消费：30"
                    , "原因：停车", "地点：大学路", "时间2019-10-10 10：02：02", "消费：20"));
            mcyadapter.notifyDataSetChanged();

        } else if (i == 4) {
            bt.setText("十一月");
            mzi.clear();
            mzi.add(new zi("原因：违章停车", "地点：学院路", "时间2019-11-1 10：50：02", "消费：50"
                    , "原因：超速行驶", "地点：学院路", "时间2019-11-2 15：00：02", "消费：50"));

            mzi.add(new zi("原因：缴费", "地点：建设路", "时间2019-11-3 10：40：22", "消费：20"
                    , "原因：缴费", "地点：南环路", "时间2019-11-4 15：20：22", "消费：30"));

            mzi.add(new zi("原因：停车", "地点：幸福路", "时间2019-11-5 10：55：12", "消费：30"
                    , "原因：停车", "地点：大学路", "时间2019-11-6 15：02：02", "消费：20"));
            mcyadapter.notifyDataSetChanged();

        }
    }

    private void setdianji() {
        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                setzhi((int) h.getX());
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void adddata() {
        mX.add("七月");
        mX.add("八月");
        mX.add("九月");
        mX.add("十月");
        mX.add("十一月");
        mY.add(new BarEntry(0, 460));
        mY.add(new BarEntry(1, 350));
        mY.add(new BarEntry(2, 420));
        mY.add(new BarEntry(3, 300));
        mY.add(new BarEntry(4, 200));

    }

    private void settu() {
        mX = new ArrayList<>();
        mY = new ArrayList<>();
        color = new ArrayList<>();
        adddata();
        dataSet = new BarDataSet(mY, "");
        color.add(Color.GREEN);
        dataSet.setColors(color);
        data = new BarData(dataSet);
        data.setBarWidth(0.3f);
        barChart.setData(data);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(mX));
        xAxis.setLabelCount(5);
        xAxis.setGranularity(1f);
        xAxis.setDrawGridLines(false);
        barChart.getAxisRight().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setDescription(null);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.getAxisLeft().setStartAtZero(true);
        barChart.getAxisLeft().setAxisMaximum(500);
        barChart.getAxisLeft().setLabelCount(10);

    }

    private void inview() {
        mfu = new ArrayList<>();
        mzi = new ArrayList<>();
        barChart = findViewById(R.id.barhart);
        bt = findViewById(R.id.bt);
        expandableListView = findViewById(R.id.ex);

        bt.setText("十一月");
        mfu.add(new fu("车辆违章"));
        mfu.add(new fu("ETC消费"));
        mfu.add(new fu("停车场消费"));
        mzi.add(new zi("原因：违章停车", "地点：学院路", "时间2019-11-1 10：50：02", "消费：50"
                , "原因：超速行驶", "地点：学院路", "时间2019-11-2 15：00：02", "消费：50"));

        mzi.add(new zi("原因：缴费", "地点：建设路", "时间2019-11-3 10：40：22", "消费：20"
                , "原因：缴费", "地点：南环路", "时间2019-11-4 15：20：22", "消费：30"));

        mzi.add(new zi("原因：停车", "地点：幸福路", "时间2019-11-5 10：55：12", "消费：30"
                , "原因：停车", "地点：大学路", "时间2019-11-6 15：02：02", "消费：20"));
        mcyadapter = new cyadapter(mfu, mzi);
        expandableListView.setAdapter(mcyadapter);
        expandableListView.expandGroup(0);
        expandableListView.expandGroup(1);
        expandableListView.expandGroup(2);
        left();
        addActivity(this);
        setTitle("创意");
    }
}
