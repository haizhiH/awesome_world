package com.example.tiku6_1.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.tiku6_1.R;
import com.example.tiku6_1.fragment.DlhjFragment;
import com.example.tiku6_1.fragment.WdlkFragment;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class AWdjtActivity extends BaseActivity {

    private TextView mLk;
    private TextView mHj;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d_wdjt_layout);
        initView();
        initLiser();
        replaceFragement(new WdlkFragment(this, getFragmentManager()));
        left();
        addActivity(this);
        setTitle("我的交通");
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_2, R.anim.anim_3);
    }

    private void initLiser() {
        mLk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLk.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                mHj.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                replaceFragement(new WdlkFragment(AWdjtActivity.this, getFragmentManager()));
            }
        });
        mHj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHj.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                mLk.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                replaceFragement(new DlhjFragment(AWdjtActivity.this));
            }
        });
    }

    private void initView() {
        mLk = findViewById(R.id.wdjt_lk);
        mHj = findViewById(R.id.wdjt_dl);
    }

    private void replaceFragement(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.wdjt_frag, fragment);
        transaction.commit();
    }
}
