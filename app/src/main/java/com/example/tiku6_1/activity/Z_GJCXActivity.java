package com.example.tiku6_1.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.bean.SSHJ;
import com.example.tiku6_1.fragment.GJCX_Fragment1;
import com.example.tiku6_1.fragment.GJCX_Fragment2;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;
import com.example.tiku6_1.util.SimpData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

public class Z_GJCXActivity extends BaseActivity {
    private TextView ztxx, hchj;
    private VolleyTo volleyTo;
    private AppClient appClient;
    private List<SSHJ> sshjs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gjcx_layout);
        initView();
        replace(new GJCX_Fragment1());
        ztxx.setTextSize(50);
        hchj.setTextSize(40);
        setOnClick();
        setVolley();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_7, R.anim.anim_8);
    }

    private void setOnClick() {
        hchj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hchj.setTextSize(50);
                ztxx.setTextSize(40);
                replace(new GJCX_Fragment2());
            }
        });
        ztxx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ztxx.setTextSize(50);
                hchj.setTextSize(40);
                replace(new GJCX_Fragment1());
            }
        });
    }

    private void replace(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();
    }


    private void setVolley() {
        volleyTo = new VolleyTo();
        volleyTo.setUrl("get_all_sense")
                .setJsonObject("UserName", "user1")
                .setLoop(true)
                .setTime(3000)
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                                if (jsonObject.optString("RESULT").equals("S")) {
                                    sshjs.add(new SSHJ(jsonObject.optInt("co2")
                                            , jsonObject.optInt("temperature")
                                            , jsonObject.optInt("humidity")
                                            , SimpData.simp("HH:mm:ss", new Date())));
                                    if (sshjs.size() == 6) {
                                        sshjs.remove(0);
                                    }
                                }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }).start();
    }

    private void initView() {
        left();
        setTitle("公交查询");
        addActivity(this);
        ztxx = findViewById(R.id.ztxx);
        hchj = findViewById(R.id.hchj);
        appClient = (AppClient) getApplication();
        sshjs = appClient.getSshjs();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sshjs.clear();
    }
}
