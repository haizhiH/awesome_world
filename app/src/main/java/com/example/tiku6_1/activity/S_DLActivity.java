package com.example.tiku6_1.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;
import com.example.tiku6_1.sql.zcSQL;

import org.json.JSONArray;
import org.json.JSONObject;

public class S_DLActivity extends AppCompatActivity {

    private EditText yhm, mm;
    private AppClient mApp;
    private zcSQL mzcSQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_dlactivity);
        inview();
        setdianjI();
    }

    private void huoqu(final String y, final String m) {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_roots").setJsonObject("UserName", "user1").setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String arr = jsonObject.getString("ROWS_DETAIL");
                    JSONArray jsonArray = new JSONArray(arr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        if (jsonObject1.getString("username").equals(y) && jsonObject1.getString("password").equals(m)) {
                            mApp.setUserName(y);
                            mApp.setPassWord(m);
                            Toast.makeText(S_DLActivity.this, "登陆成功！", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(S_DLActivity.this, Z_ZJMActivity.class));
                            finish();
                            return;
                        }
                    }
                    Toast.makeText(S_DLActivity.this, "账号不合法！", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();

        SQLiteDatabase db = mzcSQL.getReadableDatabase();
        Cursor cursor = db.query("zc", new String[]{"yhm", "yx", "mm"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String yh = cursor.getString(cursor.getColumnIndex("yhm"));
            String mm1 = cursor.getString(cursor.getColumnIndex("mm"));
            if (y.equals(yh) && m.equals(mm1)) {
                mApp.setUserName(y);
                mApp.setPassWord(m);
                startActivity(new Intent(S_DLActivity.this, S_ZCActivity.class));
                startActivity(new Intent(S_DLActivity.this, Z_ZJMActivity.class));
                finish();
            }
        }
        cursor.close();
    }

    private void setdianjI() {
        View.OnClickListener mClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.zc) {
                    startActivity(new Intent(S_DLActivity.this, S_ZCActivity.class));
                } else if (view.getId() == R.id.zhmm) {
                    startActivity(new Intent(S_DLActivity.this, S_ZHMMActivity.class));
                } else if (view.getId() == R.id.dl) {
                    huoqu(yhm.getText().toString(), mm.getText().toString());
                }
            }
        };

        findViewById(R.id.zc).setOnClickListener(mClickListener);
        findViewById(R.id.zhmm).setOnClickListener(mClickListener);
        findViewById(R.id.dl).setOnClickListener(mClickListener);
    }

    private void inview() {
        mzcSQL = new zcSQL(this);
        mApp = (AppClient) getApplication();
        yhm = findViewById(R.id.yhm);
        mm = findViewById(R.id.mm);
        if (!(mApp.getUserName().equals(""))) {
            startActivity(new Intent(S_DLActivity.this, Z_ZJMActivity.class));
            finish();
        }
    }
}
