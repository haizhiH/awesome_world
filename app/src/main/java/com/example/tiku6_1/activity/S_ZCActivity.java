package com.example.tiku6_1.activity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tiku6_1.R;
import com.example.tiku6_1.sql.zcSQL;

import java.util.regex.Pattern;

public class S_ZCActivity extends AppCompatActivity {

    private EditText yhm, mm, yx, qrmm;
    private Button tj;
    private zcSQL mzcsql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_zcactivity);
        inview();
        setdianji();
    }

    private void setdianji() {
        tj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String y = yhm.getText().toString();
                String y1 = yx.getText().toString();
                String m = mm.getText().toString();
                String m1 = qrmm.getText().toString();
                String zimu = "(.*[A-Za-z].*){4,}";
                String shuzi = "(.*[0-9].*){6,}";
                boolean matches = Pattern.matches(zimu, y);
                boolean matches1 = Pattern.matches(shuzi, y1);
                boolean matches2 = Pattern.matches(shuzi, m);
                if (!matches) {
                    Toast.makeText(S_ZCActivity.this, "用户名不少于4位字母", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!matches1) {
                    Toast.makeText(S_ZCActivity.this, "邮箱不少于6位数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!matches2) {
                    Toast.makeText(S_ZCActivity.this, "密码不少于6位数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!(m.equals(m1))) {
                    Toast.makeText(S_ZCActivity.this, "密码不一致！", Toast.LENGTH_SHORT).show();
                } else {
                    SQLiteDatabase db = mzcsql.getReadableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("yhm", y);
                    values.put("yx", y1);
                    values.put("mm", m);
                    db.insert("zc", null, values);
                    db.close();
                    Toast.makeText(S_ZCActivity.this, "注册成功！", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void inview() {
        mzcsql = new zcSQL(this);
        yhm = findViewById(R.id.yhm);
        mm = findViewById(R.id.mm);
        yx = findViewById(R.id.yx);
        qrmm = findViewById(R.id.qrmm);
        tj = findViewById(R.id.tj);
        findViewById(R.id.change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
