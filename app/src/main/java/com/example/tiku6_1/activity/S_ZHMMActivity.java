package com.example.tiku6_1.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tiku6_1.R;
import com.example.tiku6_1.sql.zcSQL;

import java.util.regex.Pattern;

public class S_ZHMMActivity extends AppCompatActivity {

    private EditText yhm, yx;
    private Button zh;
    private zcSQL mzcsql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s_zhmmactivity);
        inview();
        setdianji();
    }

    private void huoqu(String yy, String yy1) {
        SQLiteDatabase db = mzcsql.getReadableDatabase();
        Cursor cursor = db.query("zc", new String[]{"yhm", "yx", "mm"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String y = cursor.getString(cursor.getColumnIndex("yhm"));
            String y1 = cursor.getString(cursor.getColumnIndex("yx"));
            if (y.equals(yy) && y1.equals(yy1)) {
                String m = cursor.getString(cursor.getColumnIndex("mm"));
                Toast.makeText(S_ZHMMActivity.this, "找回密码为：" + m, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Toast.makeText(S_ZHMMActivity.this, "没有用户信息", Toast.LENGTH_SHORT).show();
    }

    private void setdianji() {
        zh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String y = yhm.getText().toString();
                String y1 = yx.getText().toString();
                String zimu = "(.*[A-Za-z].*){4,}";
                String shuzi = "(.*[0-9].*){6,}";
                boolean matches = Pattern.matches(zimu, y);
                boolean matches1 = Pattern.matches(shuzi, y1);
                if (!matches) {
                    Toast.makeText(S_ZHMMActivity.this, "用户名不少于4位字母", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!matches1) {
                    Toast.makeText(S_ZHMMActivity.this, "邮箱不少于6位数字", Toast.LENGTH_SHORT).show();
                    return;
                }

                huoqu(y, y1);
            }
        });
    }

    private void inview() {
        yhm = findViewById(R.id.yhm);
        yx = findViewById(R.id.yx);
        zh = findViewById(R.id.zh);
        mzcsql = new zcSQL(this);
        findViewById(R.id.change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
