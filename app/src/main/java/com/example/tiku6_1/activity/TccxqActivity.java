package com.example.tiku6_1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.R;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

public class TccxqActivity extends AppCompatActivity {

    private TextView mBt;
    private TextView mDd;
    private TextView mJl;
    private TextView mFl;
    private TextView mCw;
    private TextView mCk;
    private VolleyTo volleyTo;
    private String Name;
    private Double Jd;
    private Double Wd;
    private LinearLayout mLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tccxq_layout);
        Name=getIntent().getStringExtra("name");
        initView();
        setVolley();
        initLister();
    }

    private void initLister() {
        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(TccxqActivity.this,LxdtActivity.class);
                intent.putExtra("jd",Jd);
                intent.putExtra("wd",Wd);
                intent.putExtra("name",Name);
                startActivity(intent);
            }
        });
    }

    private void setVolley() {
        volleyTo=new VolleyTo();
        volleyTo.setUrl("get_park_data").setJsonObject("UserName","user1").setmDialog(this).setVolleyLo(new VolleyLo() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("RESULT")){
                    try {
                        String result=jsonObject.getString("RESULT");
                        if (result.equals("S")){
                            String body=jsonObject.getString("ROWS_DETAIL");
                            JSONArray jsonArray=new JSONArray(body);
                            Random random=new Random();
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                if (jsonObject1.getString("parking_name").equals(Name)){
                                    mBt.setText(jsonObject1.getString("parking_name"));
                                    mDd.setText(jsonObject1.getString("location"));
                                    mJl.setText(random.nextInt(1000)+"米");
                                    mFl.setText(jsonObject1.getString("price")+"元/"+jsonObject1.getString("rate_type"));
                                    mCw.setText(random.nextInt(600)+"个/600");
                                    mCk.setText(jsonObject1.getString("reference"));
                                    Jd=jsonObject1.getDouble("longitude");
                                    Wd=jsonObject1.getDouble("latitude");
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }).start();
    }

    private void initView() {
        mBt=findViewById(R.id.xq_name);
        mDd=findViewById(R.id.xq_dd);
        mJl=findViewById(R.id.xq_jl);
        mFl=findViewById(R.id.xq_fl);
        mCw=findViewById(R.id.xq_cw);
        mCk=findViewById(R.id.xq_ck);
        mLayout=findViewById(R.id.xq_layout);
        TextView tv=findViewById(R.id.title);
        tv.setText("停车场详情");
        findViewById(R.id.change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
