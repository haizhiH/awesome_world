package com.example.tiku6_1.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.tiku6_1.AppClient;
import com.example.tiku6_1.R;
import com.example.tiku6_1.adapter.GRAX_ADapter;
import com.example.tiku6_1.bean.GRZX;
import com.example.tiku6_1.net.VolleyLo;
import com.example.tiku6_1.net.VolleyTo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class Z_GRAXActivity extends BaseActivity {
    private ListView listView;
    private TextView right_title, my_tel, my_sex, my_name, my_userName;
    private ImageView my_image;
    private AppClient appClient;
    private String user = "";
    private List<GRZX> grzxes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grzx_layout);
        appClient = (AppClient) getApplication();
        user = appClient.getUserName();
        initView();
        setVolley();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.anim_3,R.anim.anim_4);
    }
    private void setVolley() {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_root")
                .setJsonObject("UserName",user)
                .setmDialog(this)
                .setJsonObject("Password","1234")
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                                if (jsonObject.optString("RESULT").equals("S")){
                                    /**
                                     * "id": 1,
                                     *     "name": "张三",
                                     *     "sex": "男",
                                     *     "idnumber": "385622201809083326",
                                     *     "registered_time": "1998.01.26",
                                     *     "tel": "15625895632",
                                     *     "username": "user1",
                                     *     "root": "一般管理员",
                                     *     "plate": [
                                     *         "鲁A10001",
                                     *         "鲁A10003",
                                     *         "鲁A10005"
                                     *     ]
                                     */
                                    if (jsonObject.optString("sex").equals("男")){
                                        my_image.setImageResource(R.drawable.touxiang_2);
                                    }else {
                                        my_image.setImageResource(R.drawable.touxiang_1);
                                    }
                                    my_sex.setText("性别:"+jsonObject.optString("sex"));
                                    my_tel.setText("手机:"+jsonObject.optString("tel"));
                                    my_name.setText("姓名:"+jsonObject.optString("name"));
                                    my_userName.setText("用户名:"+jsonObject.optString("username"));
                                    StringBuilder builder = new StringBuilder(jsonObject.optString("idnumber"));
                                    right_title.setText("身份证号:"+builder.replace(6,15,"*********")+"                     注册时间:"+jsonObject.optString("registered_time"));
                                    setVolley_Car();
                                }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }).start();
    }

    private void setVolley_Car() {
        VolleyTo volleyTo = new VolleyTo();
        volleyTo.setUrl("get_vehicle")
                .setJsonObject("UserName",user)
                .setVolleyLo(new VolleyLo() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                            try {
                                if ("S".equals(jsonObject.getString("RESULT"))){
                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("ROWS_DETAIL"));
                                    for (int i=0;i<jsonArray.length();i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        //brand,plate,balance;
                                        grzxes.add(new GRZX(jsonObject1.getString("brand")
                                                ,jsonObject1.getString("plate")
                                                ,jsonObject1.getString("balance")));
                                    }
                                listView.setAdapter(new GRAX_ADapter(Z_GRAXActivity.this,R.layout.grzx_listtview,grzxes));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }).start();
    }

    private void initView() {
        left();
        setTitle("我的座驾");
        addActivity(this);
        listView = findViewById(R.id.grzx_listView);
        right_title = findViewById(R.id.right_title);
        my_tel = findViewById(R.id.my_tel);
        my_sex = findViewById(R.id.my_sex);
        my_name = findViewById(R.id.my_name);
        my_userName = findViewById(R.id.my_userName);
        my_image = findViewById(R.id.my_image);
        grzxes  =new ArrayList<>();

    }
}

