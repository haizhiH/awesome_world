package com.example.tiku6_1;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.tiku6_1.bean.Lxzn;
import com.example.tiku6_1.bean.SSHJ;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2019/10/11 0011.
 */

public class AppClient extends Application {
    private static RequestQueue requestQueue;
    private static String IP = "192.168.1.101";
    private static SharedPreferences preferences;
    private List<SSHJ> sshjs = new ArrayList<>();

    public List<SSHJ> getSshjs() {
        return sshjs;
    }

    private List<Lxzn> LxList=new ArrayList<>();

    public List<Lxzn> getLxList() {
        return LxList;
    }

    public static String getIP() {
        return IP;
    }

    public static void setIP(String IP) {
        AppClient.IP = IP;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        requestQueue = Volley.newRequestQueue(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static void setRequestQueue(JsonObjectRequest jsonObjectRequest) {
        requestQueue.add(jsonObjectRequest);
    }

    public String getUserName() {
        return preferences.getString("UserName", "");
    }

    public void setUserName(String UserName) {
        preferences.edit().putString("UserName", UserName).apply();
    }

    public String getPassWord() {
        return preferences.getString("PassWord", "");
    }

    public void setPassWord(String PassWord) {
        preferences.edit().putString("PassWord", PassWord).apply();
    }
}
