package com.example.tiku6_1.util;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class Show_Dialog {
    public static void Show(Context context,String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示");
        builder.setMessage(msg);
        builder.setPositiveButton("确定",null);
        builder.show();

    }
}
