package com.example.tiku6_1.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;


import com.example.tiku6_1.R;
import com.example.tiku6_1.activity.BaseActivity;
import com.example.tiku6_1.util.SimpData;

import java.util.Date;

/**
 * Create by 张瀛煜 on 2019/10/19
 */
public class WLTS_Dialog extends DialogFragment {
    private Button exiting, setting;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.wlts_dialog, null);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setting = getView().findViewById(R.id.setting);
        exiting = getView().findViewById(R.id.exiting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
                dismiss();
            }
        });
        exiting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
               BaseActivity.setIsSuccess(true);
            }
        });
    }
}
