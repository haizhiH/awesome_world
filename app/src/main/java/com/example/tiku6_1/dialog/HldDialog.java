package com.example.tiku6_1.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.tiku6_1.R;

/**
 * Created by Administrator on 2019/10/19 0019.
 */

@SuppressLint("ValidFragment")
public class HldDialog extends DialogFragment {

    private EditText mHod;
    private EditText mHud;
    private EditText mLvd;
    private Button mOK;
    private Button mCloss;
    private ImageView mCha;
    private Context context;
    private String Lk;

    public HldDialog(Context context, String lk) {
        this.context = context;
        Lk = lk;
    }

    public interface SetData{
        void setdata(int Hod, int Hud, int Lvd);
    }
    private SetData data;

    public void setData(SetData data) {
        this.data = data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.hld_dialog,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initLister();
    }

    private void initLister() {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ho=mHod.getText().toString().trim();
                String hu=mHud.getText().toString().trim();
                String lv=mLvd.getText().toString().trim();
                if (ho.equals("")||hu.equals("")||lv.equals("")){
                    showAlertDialog("红绿灯时间不能为空");
                    return;
                }
                int hod=Integer.parseInt(ho);
                int hud=Integer.parseInt(hu);
                int lvd=Integer.parseInt(lv);
                if (!(hod>0&&hod<31)){
                    showAlertDialog("红灯时间只能为1-30");
                    return;
                }
                if (!(hud>0&&hud<31)){
                    showAlertDialog("黄灯时间只能为1-30");
                    return;
                }
                if (!(lvd>0&&lvd<31)){
                    showAlertDialog("绿灯时间只能为1-30");
                    return;
                }
                showAlertDialog(Lk+"号路口的红绿灯配置信息成功");
                data.setdata(hod,hud,lvd);
                HldDialog.this.dismiss();
            }
        });
        mCloss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HldDialog.this.dismiss();
            }
        });
        mCha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HldDialog.this.dismiss();
            }
        });
    }

    private void showAlertDialog(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("提示");
        builder.setMessage(message);
        builder.setPositiveButton("确定",null);
        builder.show();
    }

    private void initView() {
        mHod=getView().findViewById(R.id.hld_hod);
        mHud=getView().findViewById(R.id.hld_hud);
        mLvd=getView().findViewById(R.id.hld_lvd);
        mOK=getView().findViewById(R.id.hld_ok);
        mCloss=getView().findViewById(R.id.hld_closs);
        mCha=getView().findViewById(R.id.hld_cha);
    }
}
